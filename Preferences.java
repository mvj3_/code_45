// Retrieve shared preference - named preferences file
// Use MODE_WORLD_READABLE and/or MODE_WORLD_WRITEABLE to grant access to other applications
SharedPreferences preferences = getSharedPreferences("YourPreferencesName", MODE_PRIVATE);
String preferenceValue = preferences.getString("yourPreferenceKey", "defaultValue");	

// Retrieve shared preference - single preference file
// Use MODE_WORLD_READABLE and/or MODE_WORLD_WRITEABLE to grant access to other applications
SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
String preferenceValue = preferences.getString("yourPreferenceKey", "defaultValue");	

// Write shared preference - named preferences file
// Use MODE_WORLD_READABLE and/or MODE_WORLD_WRITEABLE to grant access to other applications
SharedPreferences preferences = getSharedPreferences("YourPreferencesName", MODE_PRIVATE);
SharedPreferences.Editor editor = preferences.edit();
editor.putString("yourPreferenceKey", "Your Preference Value");
editor.commit();	

// Write shared preference - single preference file
// Use MODE_WORLD_READABLE and/or MODE_WORLD_WRITEABLE to grant access to other applications
SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
SharedPreferences.Editor editor = preferences.edit();
editor.putString("yourPreferenceKey", "Your Preference Value");
editor.commit();